extends KinematicBody2D

const MAX_JUMP_HEIGHT = 600
const UP = Vector2(0, -1) # Floor
const GRAVITY = 20
const SPEED = 300

var is_jumping = false
var move_direction = Vector2()
var motion = Vector2()

func _process(delta):
	loop_control()
	loop_movement()
	loop_animation()
	pass

func loop_control():
	
	var LEFT = Input.is_action_pressed("ui_left")
	var RIGHT = Input.is_action_pressed("ui_right")
	
	if is_on_floor():
		is_jumping = Input.is_action_pressed("ui_select")
		if is_jumping:
			move_direction.y = -int(is_jumping)
			
	move_direction.x = -int(LEFT) + int(RIGHT)
	pass

func loop_movement():
	motion.y += GRAVITY
	var normal = move_direction.normalized()
	var changed_motion = Vector2(normal.x * SPEED, normal.y * MAX_JUMP_HEIGHT)
	motion.x = changed_motion.x
	
	if changed_motion.y < 0:
		if changed_motion.x == 0:
			motion.y = changed_motion.y / 1.2
		else:
			motion.y = changed_motion.y
		move_direction.y = 0
		
	motion = move_and_slide(motion, UP)
	pass

func loop_animation():
	if move_direction.x > 0:
		$anim.flip_h = false
	elif move_direction.x < 0:
		$anim.flip_h = true
	
	# Comment Remove if animation is already present in the list
#	if move_direction.x != 0:
#		$anim.play("walk")
#	else:
#		$anim.play("idle")
	pass